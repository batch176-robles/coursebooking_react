const coursesData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus molestiae alias, adipisci est repellendus cupiditate eos repudiandae quam debitis. Voluptatibus perferendis cumque quidem suscipit sequi doloribus culpa recusandae iusto inventore!`,
        price: 45000
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus molestiae alias, adipisci est repellendus cupiditate eos repudiandae quam debitis. Voluptatibus perferendis cumque quidem suscipit sequi doloribus culpa recusandae iusto inventore!`,
        price: 50000
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus molestiae alias, adipisci est repellendus cupiditate eos repudiandae quam debitis. Voluptatibus perferendis cumque quidem suscipit sequi doloribus culpa recusandae iusto inventore!`,
        price: 55000
    }
]

export default coursesData;